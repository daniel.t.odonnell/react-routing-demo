// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.
const ScheduleAPI = {
  games: [
    { id: 1, name: "@ Evergreens", date: "6/5" },
    { id: 2, name: "vs Kickers", date: "6/8" },
    { id: 3, name: "@ United", date: "6/14" }
  ],
  all: function() { return this.games},
  get: function(id) {
    return this.games.find(p => p.games === id)
  }
}

export default ScheduleAPI
