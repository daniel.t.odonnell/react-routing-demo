import React from 'react'
import PlayerAPI from '../data/player'
import { Link } from 'react-router-dom'

// The Player looks up the player using the number parsed from
// the URL's pathname. If no player is found with the given
// number, then a "player not found" message is displayed.
const Player = (props) => {
  const player = PlayerAPI.get(
    parseInt(props.match.params.number, 10)
  )
  if (!player) {
    return <div>Sorry, but the player was not found</div>
  }
  return (
    <div>
      <PlayerInfo player={player}></PlayerInfo>
      <Link to='/roster'>Back</Link>
    </div>
  )
}


class PlayerInfo extends React.Component {

   render() {
      return (
        <div>
          <h1>{this.props.player.name} (#{this.props.player.number})</h1>
          <h2>Position: {this.props.player.position}</h2>
        </div>
      );
   }
}

export default Player
