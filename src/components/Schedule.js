import React from 'react'
import ScheduleAPI from '../data/schedule'

const Schedule = () => (
  <div>
    <ul>
   {
     ScheduleAPI.all().map(p => (
       <ScheduleItem game={p}></ScheduleItem>
     ))
   }
    </ul>
  </div>
)

class ScheduleItem extends React.Component {
  render () {
    return (
      <li>Game {this.props.game.id} - {this.props.game.name} : {this.props.game.date}</li>
    )
  };
}

export default Schedule
